import {createSwitchNavigator, createStackNavigator, createAppContainer} from 'react-navigation';
import LoginScreen from "./src/LoginScreen";
import GalleryScreen from "./src/GalleryScreen";
import ProjectScreen from "./src/ProjectScreen";
import LoadingScreen from "./src/LoadingScreen";
import DemoScreen from "./src/DemoScreen";
import ImageViewSingle from "./src/ImageViewSingle";


const AppStack = createStackNavigator(
  {Home: GalleryScreen, Project: ProjectScreen, Demo: DemoScreen, ImageViewer: ImageViewSingle},
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {backgroundColor: '#000'},
      headerTintColor: '#fff',
      headerTitleStyle: {fontWeight: 'bold'}
    }
  },
);
const AuthStack = createStackNavigator({Login: LoginScreen});

const MainNavigator = createAppContainer(createSwitchNavigator(
  {
    Loading: LoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {initialRouteName: 'Loading'}
));


export default MainNavigator;
