import React from 'react';
import styled from 'styled-components';

const ButtonContainer = styled.TouchableOpacity`
	width: 30px;
	height: 30px;
	right:10px;
	background-color: ${props => props.backgroundColor};
`;

const ButtonText = styled.Text`
	font-size: 15px;
	color: ${props => props.textColor};
	text-align: center;
`;

const NavButton = props => (
  <ButtonContainer
    onPress={() => props.clickEvent()}
    backgroundColor={props.backgroundColor}
  >
    <ButtonText textColor={props.textColor}>{props.icon}</ButtonText>
  </ButtonContainer>
);

export default NavButton;
