import React, {useEffect, useState} from 'react';
import {FlatList} from 'react-native';
import Icon from 'react-native-ionicons'
import styled from 'styled-components';

const Container = styled.View`
  flex: 1;
  justify-content: center;
`;
const Item = styled.TouchableOpacity`
  flex: 1;
  flex-direction: column;
  margin: 1px;
`;
const ItemImage = styled.ImageBackground`
    justify-content: center;
    align-items: center;
    height: 200px;
    position: relative;
`;
const ItemFooter = styled.TouchableOpacity`
  position: absolute;
  bottom: 0;
  right: 8px;
`;

const ImageView = ({isCheckMode, images, getSelectedImage, navigation}) => {
  const [imageData, setData] = useState([]);
  const handleClickImage = ({id, isCheck}) => {
    setData(imageData.map(item => item.id === id ? {...item, isCheck: !isCheck} : item))
    const checkedImage = imageData.filter(image=>image.isCheck);
    getSelectedImage(checkedImage);
  };
  useEffect(() => {
    setData(images.map((url, i) => {
      return {id: i, src: url, isCheck: false};
    }));
  }, images);

  return (
    <Container>
      <FlatList
        extraData={isCheckMode} data={imageData} numColumns={2} keyExtractor={(item, index) => index}
        renderItem={({item}) => (
          <Item onPress={() => navigation.navigate('ImageViewer', item)}>
            <ItemImage source={{uri: item.src}}/>
            {isCheckMode && <ItemFooter onPress={() => handleClickImage(item)}>
              {item.isCheck ? <Icon color="#fff" name="checkbox-outline"/> : <Icon name="square-outline" color="#fff"/>}
            </ItemFooter>}
          </Item>
        )}
      />
    </Container>
  );
};

export default ImageView;