import React from 'react';
import styled from 'styled-components';
import {Button} from 'react-native'

const Container = styled.View`
	background: #fff;
	height: 290px;
	width: 90%;
	border-radius: 4px;
	margin: 16px;
	box-shadow: 0 4px 12px rgba(0, 0, 0, 0.15);
`;

const Cover = styled.View`
	width: 80%;
	height: 150px;
	left: 10%;
	box-shadow: 0 8px 16px rgba(0, 0, 0, 0.35);
`;

const Image = styled.Image`
	width: 100%;
	height: 100%;
  border-radius: 4px;
`;

const Header = styled.View`
	padding-top: 24px;
	flex-direction: column;
	align-items: center;
	height: 88px;
`;

const Title = styled.Text`
	color: #000;
	font-size: 20px;
	font-weight: 600;
`;

const SubTitle = styled.Text`
	color: #b8b3c3;
	font-size: 15px;
	font-weight: 600;
	margin-top: 4px;
`;

const CardFooter = styled.View`
  margin-top: 6px;
`;

const Card = ({clickToProject, title, date}) => {
  return (<Container>
    <Header>
      <Title>{title}</Title>
      <SubTitle>{date}</SubTitle>
    </Header>
    <Cover>
      <Image source={require('../assets/image/logo-light-black.png')}/>
    </Cover>
    <CardFooter>
      <Button title="Start to select" color="#000" onPress={()=>clickToProject()}/>
    </CardFooter>
  </Container>)
};

export default Card;