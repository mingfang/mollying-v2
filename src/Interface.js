import React, {StatelessComponent} from 'react';

interface NavStatelessComponent extends StatelessComponent {
  navigationOptions?: Object
}

export default NavStatelessComponent;