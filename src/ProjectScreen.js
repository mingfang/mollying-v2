import React, {useEffect} from 'react';
import {Button, Alert} from 'react-native';
import type NavStatelessComponent from "./Interface";
import ImageView from "./components/ImageView";
import {demoData} from "./constant";

const modeDic = {Select: "Done", Done: "Select"};

const ProjectScreen: NavStatelessComponent = ({navigation}) => {
  const navMode = navigation.getParam('mode', 'Select');
  const selectedImage = [];
  const getSelectedImage = (images) => {
    Object.assign(selectedImage, images);
  };

  //Process Selected Image.
  // const submitSelectedImage = () => {
  //
  // };

  return (
    <ImageView isCheckMode={navMode === "Done"} images={demoData} getSelectedImage={getSelectedImage}
               navigation={navigation}/>
  );
};

ProjectScreen.navigationOptions = ({navigation}) => {
  const navTitle = navigation.getParam('title', 'Project Title');
  const navMode = navigation.getParam('mode', 'Select');
  return {
    headerTitle: navTitle,
    headerRight: (
      <Button title={navMode} color="#fff" onPress={() => {
        navigation.setParams({mode: modeDic[navMode]})
      }}/>
    ),
  }
};

export default ProjectScreen;
