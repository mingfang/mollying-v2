import React from 'react';
import {Container} from 'native-base';
import Card from "./components/Card";
import {Button} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const GalleryScreen = ({navigation}) =>
  <Container>
    <Card title="Sky" date="06/06/2019" clickToProject={() =>
      navigation.navigate('Project', {
        title: "Sky",
        mode: 'Select'
      })
    }/>
  </Container>;

GalleryScreen.navigationOptions = ({navigation}) => {
  return {
    headerTitle: 'Gallery',
    headerLeft: (
      <Button
        onPress={async () => {
          await AsyncStorage.clear();
          navigation.navigate('Auth');
        }}
        title="Logout"
        color="#fff"
      />
    ),
    headerRight: (
      <Button
        onPress={()=>navigation.navigate('Demo')}
        title="Portfolio"
        color="#fff"
      />
    )
  }
};

export default GalleryScreen;
