import React from 'react';
import styled from 'styled-components';
import {View} from 'react-native'
import {Button, Input, Item, Text} from 'native-base';
import DarkLogo from './assets/image/logo-dark.png';
import AsyncStorage from '@react-native-community/async-storage';
import type NavStatelessComponent from "./Interface";

const Container = styled.View`
  flex: 1;
  padding: 10%;
`;

const Logo = styled.Image`
  flex: 1;
  width: 100%;
  resize-mode: contain;
  align-self: center;
`;

const FormArea = styled.View`
  flex: 1;
  width: 100%;
  justify-content: space-evenly;
`;

const LoginScreen: NavStatelessComponent = ({navigation}) => {
  const signIn = async () => {
    await AsyncStorage.setItem('userToken', 'abc');
    navigation.navigate('App');
  };

  return (
    <Container>
      <Logo source={DarkLogo}/>
      <FormArea>
        <View>
          <Item regular>
            <Input placeholder='Username'/>
          </Item>
          <Item regular style={{marginTop: 8}}>
            <Input placeholder='Password'/>
          </Item>
        </View>
        <Button full dark onPress={() => signIn()}>
          <Text> Login </Text>
        </Button>
      </FormArea>
    </Container>
  );
};

LoginScreen.navigationOptions = {
  header: null,
};

export default LoginScreen;
