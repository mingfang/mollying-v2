import React from 'react';
import GallerySwiper from 'react-native-gallery-swiper';
import {demoData} from "./constant";

const DemoScreen = ({images}) => {
  const data = images || demoData;
  return <GallerySwiper images={data.map(item=>({uri:item}))}/>
};

export default DemoScreen;