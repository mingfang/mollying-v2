import React from 'react';
import GallerySwiper from "react-native-gallery-swiper";

const ImageViewSingle = ({navigation}) => {
  const uri = navigation.getParam('src', '');
  return <GallerySwiper images={[
    {uri}
  ]}/>
};

export default ImageViewSingle;